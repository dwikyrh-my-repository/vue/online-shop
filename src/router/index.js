// import vue router
import { createRouter, createWebHistory } from "vue-router";

// import store vuex
import store from "@/store";

// define a routes
const routes = [
  {
    path: "/register",
    name: "register",
    component: () => import("../views/auth/Register.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/auth/Login.vue"),
  },
  {
    path: "/customer/dashboard",
    name: "dashboard",
    component: () => import("../views/dashboard/Index.vue"),
    // check is loggedIn
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/customer/order",
    name: "order",
    component: () => import("../views/order/Index.vue"),
    meta: {
      // check is loggedIn
      requiresAuth: true
    }
  },
  {
    path: "/customer/order/:snap_token",
    name: "detail_order",
    component: () => import("../views/order/Show.vue"),
    meta: {
      //chek is loggedIn
      requiresAuth: true
    }
  },
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home/Index.vue')
  },
  {
    path: '/product/:slug',
    name: 'detail_product',
    component: () => import('../views/products/Show.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    component: () => import('../views/categories/Index.vue')
  },
  {
    path: '/category/:slug',
    name: 'detail_category',
    component: () => import('../views/categories/Show.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    component: () => import('../views/carts/Index.vue'),
    meta: {
      //chek is loggedIn
      requiresAuth: true
    }
  }
];

// create router
const router = createRouter({
  history: createWebHistory(),
  routes, // routes
});

// define route for handle authentication
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // check nilai dari getters is LoggedIn di module auth
    if (store.getters["auth/isLoggedIn"]) {
      next();
      return;
    }
    next("/login");
  } else {
    next();
  }
});
export default router;

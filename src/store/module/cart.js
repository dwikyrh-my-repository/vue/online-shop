// import global API
import Api from "../../api/Api";

const cart = {
  // set namespaced true
  namespaced: true,

  // state
  state: {
    // cart
    cart: [],

    // total cart
    cartTotal: 0,

    // cart weight
    cartWeight: 0,
  },

  // mutations
  mutations: {
    // get data cart
    GET_CART(state, product) {
      state.cart = product;
    },

    // get cart total
    TOTAL_CART(state, total) {
      state.cartTotal = total;
    },

    // get cart weight
    CART_WEIGHT(state, weight) {
      state.cartWeight = weight;
    },

    // clear all cart
    CLEAR_CART(state) {
      state.cart = [];
      state.cartTotal = 0;
      state.cartWeight = 0;
    },
  },

  // actions
  actions: {
    // action add to cart
    addToCart({ commit }, { product_id, price, quantity, weight }) {
      //get data token dan user
      const token = localStorage.getItem("token");
      const user = JSON.parse(localStorage.getItem("user"));

      //set axios header dengan type Authorization + Bearer token
      if (token) {
        Api.defaults.headers.common["Authorization"] = "Bearer" + token;
      }
      // send data cart to server
      Api.post("/carts", {
        product_id: product_id,
        price: price,
        quantity: quantity,
        weight: weight,
        customer_id: user.id,
      })
        .then(() => {
          // get data cart
          Api.get("/carts").then((response) => {
            // commit mutations GET_CART
            commit("GET_CART", response.data.cart);
          });
          // get total cart
          Api.get("/carts/total").then((response) => {
            // commit mutations TOTAL_CART
            commit("TOTAL_CART", response.data.total);
          });
        })
        .catch((error) => {
          console.log(error);
        });
    },

    // cart count
    cartCount({ commit }) {
      // get data token and user
      const token = localStorage.getItem("token");

      // //set axios header dengan type Authorization + Bearer token
      Api.defaults.headers.common["Authorization"] = "Bearer " + token;

      // get data cart
      Api.get("/carts")
        .then((response) => {
          // commit mutations GET_CART
          commit("GET_CART", response.data.cart);
        })
        .catch((error) => {
          console.log(error);
        });
    },

    // cart total
    cartTotal({ commit }) {
      // get data token and user
      const token = localStorage.getItem("token");

      // set axios header dengan type Authorization + Bearer token
      Api.defaults.headers.common["Authorization"] = "Bearer " + token;

      // get data cart
      Api.get("/carts/total").then((response) => {
        // commit mutations GET_CART
        commit("TOTAL_CART", response.data.total);
      });
    },

    // cart weight
    cartWeight({ commit }) {
      // get data token dan user
      const token = localStorage.getItem("token");

      // set axios header dengan type Authorization + Bearer token
      Api.defaults.headers.common["Authorization"] = "Bearer " + token;

      Api.get("/carts/totalWeight").then((response) => {
        // commit mutations CART_WEIGHT
        commit("CART_WEIGHT", response.data.total);
      });
    },

    // action remove cart
    removeCart({ commit }, cart_id) {
      // get data token and user
      const token = localStorage.getItem("token");

      // set axios header dengan type Authorization + Bearer token
      Api.defaults.headers.common["Authorization"] = "Bearer " + token;

      Api.post("/carts/remove", {
        cart_id: cart_id,
      }).then(() => {
        // get cart
        Api.get("/carts").then((response) => {
          commit("GET_CART", response.data.cart);
        });

        // get total cart
        Api.get("/carts/total").then((response) => {
          commit("TOTAL_CART", response.data.total);
        });

        // get total weight
        Api.get("/carts/totalWeight").then((response) => {
          commit("CART_WEIGHT", response.data.total);
        });
      });
    },

    // checkout
    checkout({ commit }, data) {
      return new Promise((resolve) => {
        Api.post("/checkout", {
          courier: data.courier_type,
          service: data.courier_service,
          cost: data.courier_cost,
          weight: data.weight,
          name: data.name,
          phone: data.phone,
          province: data.province_id,
          city: data.city_id,
          address: data.address,
          grand_total: data.grandTotal,
        }).then((response) => {
          resolve(response.data);

          // remove all cart on database
          Api.post("/carts/removeAll")
            .then(() => {
              // clear cart
              commit("CLEAR_CART");
            })
            .catch((error) => {
              console.log(error);
            });
        });
      });
    },
  },
  // getters
  getters: {
    // get cart
    getCart(state) {
      return state.cart;
    },

    // count cart
    cartCount(state) {
      return state.cart.length;
    },

    // cart total
    cartTotal(state) {
      return state.cartTotal;
    },
  },
};

export default cart;

// import vuex
import {createStore} from 'vuex'

import auth from './module/auth'

import order from './module/order'

import category from './module/category'

import slider from './module/slider'

import product from './module/product'

import cart from './module/cart'

// create store vuex
export default createStore({
  modules: {
    auth, // module auth
    order, // module order
    category, // module category
    slider, // module slider
    product, // module product
    cart, // module cart
  }
})
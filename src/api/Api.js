// import axios
import axios from "axios";

const Api = axios.create({
  // set default endpoint api
  baseURL: "https://greenk-online-shop.online/api",
  // baseURL: "http://localhost:8000/api",
});
export default Api;
